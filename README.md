# Flabio starter html

Стартовый шаблон для нашей компании.
В шаблон включены все необходимые плагины для сборки проектов на **html**


---


# Обязательные программы!

- **[node.js](https://nodejs.org/en/)** и пакетный менеджер **[npm](https://nodejs.org/en/)**
- **[git](https://git-scm.com/)**
- **[ImageMagick](http://www.imagemagick.org/script/download.php)**


---


# Для запуска необходимо запустить следующие команды в консоли

 Установка Gulp, Bower глобально
```
$ npm install --global gulp-cli bower
```
Откройте терминал в директории проекта и запустите
```
$ npm install && bower install
```
Запустите проект
```
$ gulp serve
```
Проект будет открыт по следующей ссылке http://localhost:3922/


---

# Подключение иконок fontello

1) Название шрифтов и иконок по скрину ![ Название шрифтов и иконок по скрину ](http://screen.flabio.tech/aiaz-loChJbOc-04-2018.png)
2) Удалить подключение шрифтов ![ Удалить подключение шрифтов ](http://screen.flabio.tech/aiaz-h9mk5yr--04-2018.png)

---

# Сборка проекта

```
$ gulp
```

---

# Пересборка стилей и скриптов

```
$ gulp wiredep
```

---

# Превью для картинок (больших размеров)

```
$ gulp crop
```

---

# Ленивая загрузка **изображений**

Для **img**
```
<img src="images/thumbsize/test.jpg" data-original="images/originsize/test.jpg" alt="" class="flabLazy">
```

Для **background**
```
<div class="flabLazy" data-original="images/originsize/test.jpg" style="background-image: url(images/thumbsize/test.jpg);"></div>
```

---

---

# Плагины Jquery

- [bootstrap 4](https://getbootstrap.com/)
- [modernizr](https://modernizr.com/)
- [jquery](https://jquery.com/)
- [jquery_lazyload](https://appelsiini.net/projects/lazyload/)

---

# Плагины **npm**

- babel-core
- babel-preset
- babel-register
- browser-sync
- del
- gulp
- gulp-autoprefixer
- gulp-babel
- gulp-cache
- gulp-cssnano
- gulp-eslint
- gulp-filter
- gulp-htmlmin
- gulp-if
- gulp-imagemin
- gulp-load
- gulp-notify
- gulp-plumber
- gulp-sass
- gulp-size
- gulp-sourcemaps
- gulp-uglify
- gulp-useref
- gulp-wait
- main-bower
- run-sequence
- wiredep
- gulp-image-resize
- gulp-js_obfuscator

---

# Специально для команды [Flabio](https://flabio.kg/)
